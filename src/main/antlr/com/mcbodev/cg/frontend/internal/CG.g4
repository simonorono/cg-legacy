/*
 * Copyright 2016 Simón Oroño & La Universidad del Zulia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

grammar CG;

/* Naming purpose only */
ADD : '+' ;
ADD_ASSIGN : '+=' ;
AND : '&&' ;
ASSERT : 'assert' ;
BANG : '!' ;
BOOL : 'bool' ;
BREAK : 'break' ;
COMMA : ',' ;
CONTINUE : 'continue' ;
DIGRAPH : 'digraph' ;
DIV : '/' ;
DIV_ASSIGN : '/=' ;
DOT : '.' ;
ELIF : 'elif' ;
ELSE : 'else' ;
EQUAL : '=' ;
EQUAL_EQUAL : '==' ;
FLOAT : 'float' ;
FOR : 'for' ;
FUNC : 'func' ;
GE : '>=' ;
GRAPH : 'graph' ;
GT : '>' ;
IF : 'if' ;
IN : 'in' ;
INCLUDE : 'include' ;
INT : 'int' ;
LBRACE : '{' ;
LBRACK : '[' ;
LE : '<=' ;
LPAREN : '(' ;
LT : '<' ;
MOD : '%' ;
MOD_ASSIGN : '%=' ;
MUL : '*' ;
MUL_ASSIGN : '*=' ;
NOT_EQUAL : '!=' ;
OR : '||' ;
RANGE : '..' ;
RBRACE : '}' ;
RBRACK : ']' ;
RETURN : 'return' ;
RPAREN : ')' ;
SEMI : ';' ;
STRING : 'string' ;
STRUCT : 'struct' ;
SUB : '-' ;
SUB_ASSIGN : '-=' ;
VAR : 'var' ;
VOID : 'void' ;
WHILE : 'while' ;

primitive: 'int'
         | 'float'
         | 'bool'
         | 'string'
         | 'graph'
         | 'digraph'
         | 'void'
         ;
type: primitive    #PrimitiveType
    | '[' type ']' #ArrayType
    | IDENTIFIER   #UserType
    ;

edge: '[' source=expr ',' target=expr ']';
graphLit: gtype=('graph'|'digraph') '[' num=expr ']' ('{' (edge ( ',' edge)*)? '}')?;

param: IDENTIFIER type;
paramList: (param (',' param)*)?;

statement: varDec ';'               #LocalVarDec
         | expr ';'                 #Stmt
         | compound                 #CompoundStmt
         | 'return' expr? ';'       #Return
         | ('break'|'continue') ';' #Control
         ;
block: '{' (statement)* '}';

lit: INT_LIT    #IntLit
   | FLOAT_LIT  #FloatLit
   | BOOL_LIT   #BoolLit
   | STRING_LIT #StringLit
   ;

elifc: 'elif' '(' expr ')' block ;
elsec: 'else' block ;

loopc: 'for' '(' variable=expr 'in' collection=expr ')' block #For
     | 'while' '(' expr ')' block                             #While
     ;

compound: 'if' '(' expr ')' block elifc* elsec? #IfExpr
        | loopc                                 #LoopExpr
        ;

atom: lit               #LiteralExpr
    | block             #BlockExpr
    | IDENTIFIER        #VarExpr
    | graphLit          #GraphExpr
    | arrayLit          #ArrayExpr
    | compound          #CompoundExpr
    ;

postfixExpression: atom                                #Atomic
                 | postfixExpression '.' IDENTIFIER    #PropAccess
                 | postfixExpression '[' expr ']'      #ArrayAccess
                 | postfixExpression '(' exprList? ')' #FunctionCall
                 | '(' type ')' postfixExpression      #CastExpr
                 ;

expr: postfixExpression                                #Postfix
    | <assoc=right> op=('-'|'!') expr                  #Unary
    | '(' expr ')'                                     #Assoc
    | expr op=('*'|'/'|'%') expr                       #MulDivMod
    | expr op=('+'|'-') expr                           #AddSub
    | expr op=('>'|'<'|'>='|'<=') expr                 #Comparison
    | expr op=('=='|'!=') expr                         #Equality
    | expr op='&&' expr                                #LogicAnd
    | expr op='||' expr                                #LogicOr
    | <assoc=right> expr '=' expr                      #Assignment
    | <assoc=right> expr op=('*='|'+='|'-='|'/=') expr #AssignmentAcc
    | from=expr '..' to=expr                           #Range
    ;

exprList: (expr (',' expr)*) ;
arrayLit: '[' exprList? ']';

varDec: 'var' IDENTIFIER type? ('=' expr)? ;

funcDef: 'func' IDENTIFIER '(' paramList ')' type? (block | '=' expr ';');

unit: (funcDef | varDec ';')+;

/* Non-visible */
WHITESPACE: [ \t\r\n] -> skip;
COMMENT : '//' (.)*? '\n' -> skip;
ML_COMMENT : '/*' (.)*? '*/' -> skip ;

/* Literals */
BOOL_LIT: 'true' | 'false';

fragment LETTER: [a-zA-Z_];
fragment DIGIT: [0-9];

IDENTIFIER: LETTER (LETTER | DIGIT)*;

INT_LIT: DIGIT+;

fragment EXPONENT: [eE] [+-]? INT_LIT;
FLOAT_LIT: INT_LIT? '.' INT_LIT EXPONENT?;

fragment ESC: '\\' [tbnr"'\\];
fragment CHR: ~[\\'"];

STRING_LIT: '"' (ESC|CHR)*? '"';
