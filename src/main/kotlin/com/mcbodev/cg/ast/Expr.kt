/*
 * Copyright 2016 Simón Oroño & La Universidad del Zulia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mcbodev.cg.ast

import com.mcbodev.cg.lang.Operator
import com.mcbodev.cg.lang.PrimitiveType
import com.mcbodev.cg.lang.Type

sealed class Expr : Statement() {
    var type: Type = PrimitiveType.UNKNOWN
    var assignable = false
}

data class ArrayAccess(
    val array: Expr,
    val subscript: Expr
) : Expr() {
    override lateinit var location: Location

    init {
        assignable = true
    }

    constructor(array: Expr, subscript: Expr, location: Location) :
            this(array, subscript) {
        this.location = location
    }
}

data class ArrayLiteral(val exprs: List<Expr>) : Expr() {
    override lateinit var location: Location

    constructor(exprs: List<Expr>, location: Location) : this(exprs) {
        this.location = location
    }
}

data class Assignment(
    val lhs: Expr,
    val rhs: Expr
) : Expr() {
    override lateinit var location: Location

    constructor(lhs: Expr, rhs: Expr, location: Location) : this(lhs, rhs) {
        this.location = location
    }
}

data class BinaryOp(
    val op: Operator,
    val lhs: Expr,
    val rhs: Expr
) : Expr() {
    override lateinit var location: Location

    constructor(op: Operator, lhs: Expr, rhs: Expr, location: Location) :
            this(op, lhs, rhs) {
        this.location = location
    }
}

data class Cast(
    val target: Type,
    val expr: Expr
) : Expr() {
    override lateinit var location: Location

    init {
        type = target
    }

    constructor(target: Type, expr: Expr, location: Location) :
            this(target, expr) {
        this.location = location
    }
}

data class Edge(
    val from: Expr,
    val to: Expr
)

data class FuncCall(
    val function: Expr,
    val args: List<Expr>
) : Expr() {
    override lateinit var location: Location

    constructor(function: Expr, args: List<Expr>, location: Location) :
            this(function, args) {
        this.location = location
    }
}

data class GraphLit(
    val num: Expr,
    val edges: List<Edge>
) : Expr() {
    override lateinit var location: Location

    constructor(num: Expr, edges: List<Edge>, location: Location) :
            this(num, edges) {
        this.location = location
    }
}

data class Literal(val text: String) : Expr() {
    override lateinit var location: Location

    constructor(text: String, location: Location) : this(text) {
        this.location = location
    }
}

data class PropAccess(
    val expr: Expr,
    val property: String
) : Expr() {
    override lateinit var location: Location

    constructor(expr: Expr, property: String, location: Location) :
            this(expr, property) {
        this.location = location
    }
}

data class Range(
    val from: Expr,
    val to: Expr
) : Expr() {
    override lateinit var location: Location

    constructor(from: Expr, to: Expr, location: Location) : this(from, to) {
        this.location = location
    }
}

data class UnaryOp(
    val op: Operator,
    val expr: Expr
) : Expr() {
    override lateinit var location: Location

    constructor(op: Operator, expr: Expr, location: Location) : this(op, expr) {
        this.location = location
    }
}


data class Variable(val id: String) : Expr() {
    override lateinit var location: Location

    init {
        assignable = true
    }

    constructor(id: String, location: Location) : this(id) {
        this.location = location
    }
}
