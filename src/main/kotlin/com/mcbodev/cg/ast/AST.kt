/*
 * Copyright 2016 Simón Oroño & La Universidad del Zulia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mcbodev.cg.ast

import com.mcbodev.cg.lang.Type

enum class ControlType {
    CONTINUE,
    BREAK;

    override fun toString() = when (this) {
        CONTINUE -> "continue"
        BREAK -> "break"
    }
}

data class Point(val line: Int, val column: Int) {
    override fun toString() = "($line:$column)"
}

data class Location(val begin: Point) {
    override fun toString() = "$begin"
}

abstract class Node {
    abstract val location: Location
    var parent: Node = NoParent
}

object NoParent : Node() {
    override val location = Location(Point(-1, -1))
}

abstract class Statement : Node() {
    var returns = false
}

data class If(
    val cond: Expr,
    val body: Block,
    val elifs: List<Elif>,
    val elsec: Else?
) : Statement() {
    override lateinit var location: Location

    constructor(
        cond: Expr,
        body: Block,
        elifs: List<Elif>,
        elsec: Else?,
        location: Location
    ) : this(cond, body, elifs, elsec) {
        this.location = location
    }

    init {
        body.stmt.forEach { it.parent = this }
    }
}

data class Elif(
    val cond: Expr,
    val body: Block
) : Node() {
    override lateinit var location: Location

    constructor(
        cond: Expr,
        body: Block,
        location: Location
    ) : this(cond, body) {
        this.location = location
    }

    init {
        body.stmt.forEach { it.parent = this }
    }
}

data class Else(
    val body: Block
)

data class Param(val id: String, val type: Type) : Node() {
    override lateinit var location: Location

    constructor(id: String, type: Type, location: Location) : this(id, type) {
        this.location = location
    }
}

data class Block(val stmt: List<Statement>) : Node() {
    override lateinit var location: Location

    constructor(stmts: List<Statement>, location: Location) : this(stmts) {
        this.location = location
    }
}

data class FuncDef(
    val id: String,
    val params: List<Param>,
    val type: Type,
    val body: Block
) : Node() {
    override lateinit var location: Location

    constructor(
        id: String,
        params: List<Param>,
        type: Type,
        body: Block,
        location: Location
    ) : this(id, params, type, body) {
        this.location = location
    }

    init {
        params.forEach { it.parent = this }
        body.stmt.forEach { it.parent = this }
    }
}

data class VarDec(
    val id: String,
    val type: Type,
    val init: Expr?
) : Statement() {
    override lateinit var location: Location

    constructor(id: String, type: Type, init: Expr?, location: Location) :
            this(id, type, init) {
        this.location = location
    }
}

data class Return(val expr: Expr?) : Statement() {
    override lateinit var location: Location

    init {
        returns = true
    }

    constructor(expr: Expr?, location: Location) : this(expr) {
        this.location = location
    }
}

data class Control(val type: ControlType) : Statement() {
    override lateinit var location: Location

    constructor(type: ControlType, location: Location) : this(type) {
        this.location = location
    }
}

data class For(
    val variable: Expr,
    val collection: Expr,
    val body: Block
) : Statement() {
    override lateinit var location: Location

    constructor(
        variable: Expr,
        collection: Expr,
        body: Block,
        location: Location
    ) : this(variable, collection, body) {
        this.location = location
    }

    init {
        body.stmt.forEach { it.parent = this }
    }
}

data class While(
    val condition: Expr,
    val body: Block
) : Statement() {
    override lateinit var location: Location

    constructor(cond: Expr, body: Block, location: Location) :
            this(cond, body) {
        this.location = location
    }

    init {
        body.stmt.forEach { it.parent = this }
    }
}

data class CompilationUnit(
    val funcDefs: List<FuncDef>,
    val varDecs: List<VarDec>
) : Node() {
    override lateinit var location: Location

    constructor(
        funcDefs: List<FuncDef>,
        varDecs: List<VarDec>,
        location: Location
    ) : this(
        funcDefs,
        varDecs
    ) {
        this.location = location
    }

    init {
        funcDefs.forEach { it.parent = this }
        varDecs.forEach { it.parent = this }
    }
}

data class Property(
    val id: String,
    val type: Type
)
