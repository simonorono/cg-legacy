/*
 * Copyright 2016 Simón Oroño & La Universidad del Zulia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mcbodev.cg

import com.mcbodev.cg.exception.CGCException
import org.apache.commons.cli.*
import kotlin.system.exitProcess

private val options = Options().apply {
    addOption(
        Option.builder("h")
            .longOpt("help")
            .desc("Print this help output")
            .build()
    )

    addOption(
        Option.builder("c")
            .longOpt("check")
            .desc("Just check the code for errors")
            .build()
    )

    addOption(
        Option.builder("v")
            .longOpt("verbose")
            .desc("Print extra output")
            .build()
    )
}

private fun printUsage() {
    val hf = HelpFormatter()
    hf.printHelp("cg [options] args", options)
}

fun main(args: Array<String>) {
    val cmdParser = DefaultParser()

    try {
        val cmd = cmdParser.parse(options, args)

        if (cmd.hasOption("h")) {
            printUsage()
            exitProcess(0)
        }

        Log.debug = cmd.hasOption('v')

        if (cmd.args.isNotEmpty()) {
            try {
                Runner.run(cmd.args[0], onlyCheck = cmd.hasOption('c'))
            } catch (e: CGCException) {
                println(e.message)
            }
        } else {
            printUsage()
            exitProcess(0)
        }

    } catch (e: UnrecognizedOptionException) {
        println(e.message)
        printUsage()
    }
}
