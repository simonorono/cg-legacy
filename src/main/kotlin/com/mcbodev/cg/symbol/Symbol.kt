/*
 * Copyright 2016 Simón Oroño & La Universidad del Zulia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mcbodev.cg.symbol

import com.mcbodev.cg.ast.Location
import com.mcbodev.cg.lang.Type

/**
 * Base class for all symbols.
 *
 * @property id The identifier of every symbol
 * @property location The place in the source code on which the symbol is
 *                    defined
 */
sealed class Symbol {
    abstract val id: String
    abstract val location: Location
}

/**
 * Represents variables in the symbol table.
 *
 * @property type The date type of the variable
 * @property scope The scope of the variable
 * @constructor Creates a variable with specified id, type, scope and location
 */
data class Variable(
    override val id: String,
    val type: Type,
    override val location: Location
) : Symbol()

/**
 * A symbol that represents a function in a program.
 */
data class Function(
    override val id: String,
    val type: Type,
    val params: List<Variable>,
    override val location: Location
) : Symbol()
