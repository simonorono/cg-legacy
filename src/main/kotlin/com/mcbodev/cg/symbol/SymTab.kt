/*
 * Copyright 2016 Simón Oroño & La Universidad del Zulia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mcbodev.cg.symbol

import com.mcbodev.cg.ast.Node

class SymTab {
    private val funs = arrayListOf<Function>()
    private val vars = hashMapOf<Node, HashSet<Variable>>()

    fun findVariable(id: String, scope: Node?): Variable? {
        var iter = scope
        while (iter != null) {
            val v = vars[iter]?.firstOrNull { it.id == id }
            v?.let { return v }
            iter = iter.parent
        }
        return null
    }

    fun findFunction(id: String) = funs.firstOrNull { it.id == id }

    fun addFn(f: Function) {
        funs += f
    }

    fun addVar(v: Variable, scope: Node) {
        val set = vars[scope]
        if (set != null) {
            set += v
        } else {
            vars[scope] = hashSetOf()
        }
    }
}
