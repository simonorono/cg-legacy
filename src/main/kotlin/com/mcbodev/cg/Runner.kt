/*
 * Copyright 2016 Simón Oroño & La Universidad del Zulia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mcbodev.cg

import com.mcbodev.cg.frontend.ANTLRParser
import com.mcbodev.cg.frontend.passes.Checker
import com.mcbodev.cg.frontend.passes.Globals
import com.mcbodev.cg.frontend.passes.Structure
import com.mcbodev.cg.frontend.passes.Types
import java.io.File

object Runner {
    fun run(
        fileName: String,
        onlyCheck: Boolean = false
    ) {

        val file = File(fileName)
        val input = file.inputStream()
        val parser = ANTLRParser()
        val ast = parser.parse(input)

        val checker = Checker()

        checker += ::Globals
        checker += ::Structure
        checker += ::Types
        val status = checker.check(ast)

        if (status.errors.isNotEmpty()) {
            println("There were errors in code: ")
            status.errors.forEach {
                println(it.message)
                println()
            }
        }

        if (!onlyCheck) {
            // Execute here
        }
    }
}
