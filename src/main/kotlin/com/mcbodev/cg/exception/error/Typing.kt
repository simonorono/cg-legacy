package com.mcbodev.cg.exception.error

import com.mcbodev.cg.ast.*
import com.mcbodev.cg.exception.CGCException
import com.mcbodev.cg.lang.Type
import com.mcbodev.cg.symbol.Variable as VariableSym

class ArrayAccessIndexNotInteger(arrayAccess: ArrayAccess) : CGCException() {
    override val message = """
        |${arrayAccess.location}:
        |  array subscript must be integer
    """.trimMargin()
}

class AssignmentTypeMismatch(assignment: Assignment) : CGCException() {
    override val message = """
        |${assignment.location}:
        |  expected value of type ${assignment.lhs.type}; ${assignment.rhs.type} given
    """.trimMargin()
}

class BinaryOperationDoesNotExists(
    binaryOp: BinaryOp,
    lhsType: Type,
    rhsType: Type
) : CGCException() {
    override val message = """
        |${binaryOp.location}:
        |  invalid operation ${binaryOp.op} between types $lhsType and $rhsType
    """.trimMargin()
}

class InvalidCast(cast: Cast, source: Type) : CGCException() {
    override val message = """
        |${cast.location}:
        |  invalid cast from type $source to type ${cast.target}
    """.trimMargin()
}

class LHSNotAssignable(assignment: Assignment) : CGCException() {
    override val message = """
        |${assignment.location}:
        |  left side of assignment not assignable
    """.trimMargin()
}

class LocalVariableRedeclaration(
    orig: VariableSym,
    redc: VarDec
) : CGCException() {

    override val message = """
        |${redc.location}:
        |  redeclaration of local variable ${orig.id}
        |  already declared at ${orig.location}
    """.trimMargin()
}

class NonHomogeneousArray(arrayLiteral: ArrayLiteral) : CGCException() {
    override val message = """
        |${arrayLiteral.location}:
        |  elements in array literal are not of the same type
    """.trimMargin()
}

class TypeOfVariableNotInferable(varDec: VarDec) : CGCException() {
    override val message = """
        |${varDec.location}:
        |  could not infer the type of variable ${varDec.id}
    """.trimMargin()
}

class UnaryOperationDoesNotExists(unaryOp: UnaryOp, valType: Type) :
    CGCException() {
    override val message = """
        |${unaryOp.location}:
        |  invalid operation ${unaryOp.op} on type $valType
    """.trimMargin()
}

class VariableNotFound(variable: Variable) : CGCException() {
    override val message = """
        |${variable.location}:
        |  could not find variable ${variable.id} in current scope
    """.trimMargin()
}
