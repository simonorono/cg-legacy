/*
 * Copyright 2016 Simón Oroño & La Universidad del Zulia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mcbodev.cg.exception.error

import com.mcbodev.cg.ast.FuncDef
import com.mcbodev.cg.ast.VarDec
import com.mcbodev.cg.exception.CGCException
import com.mcbodev.cg.symbol.Function
import com.mcbodev.cg.symbol.Variable

/**
 * Error triggered when a function is redefined (same name).
 *
 * @param orig The original function
 * @param redf The duplicated function
 */
class FunctionRedefinition(
    orig: Function,
    redf: FuncDef
) : CGCException() {

    override val message = """
        |${redf.location}:
        |  redefinition of function ${orig.id}
        |  already defined at ${orig.location}
    """.trimMargin()
}

/**
 * Error triggered when a global variable is redefined (same name)
 *
 * @param orig The original global variable
 * @param redc The duplicated global variable
 */
class GlobalVariableRedeclaration(
    orig: Variable,
    redc: VarDec
) : CGCException() {

    override val message = """
        |${redc.location}:
        |  redeclaration of global variable ${orig.id}
        |  already declared at ${orig.location}
    """.trimIndent()
}