package com.mcbodev.cg.exception.error

import com.mcbodev.cg.ast.Control
import com.mcbodev.cg.ast.FuncDef
import com.mcbodev.cg.exception.CGCException

/**
 * Error triggered when a control statment is used outside a loop
 *
 * @param ctrl The invalid control
 */
class ControlNotInLoop(ctrl: Control) : CGCException() {
    override val message = """
        |${ctrl.location}
        |  control statement `${ctrl.type}` not inside loop
    """.trimMargin()
}

/**
 * Error triggered when a function expecting to return does not return.
 *
 * @param funcDef The function with the error
 */
class NotAllPathsReturns(funcDef: FuncDef) : CGCException() {
    override val message = """
        |${funcDef.location}:
        |  not all paths returns in non-void function `${funcDef.id}`
    """.trimMargin()
}

/**
 * Error triggered when a function with return type `void` has a non-empty
 * return.
 *
 * @param funcDef The function with the error
 */
class ReturnInVoidFunction(funcDef: FuncDef) : CGCException() {
    override val message = """
        |${funcDef.location}:
        |  return in non-void function `${funcDef.id}`
    """.trimMargin()
}
