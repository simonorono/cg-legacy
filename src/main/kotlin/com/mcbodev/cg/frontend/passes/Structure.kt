package com.mcbodev.cg.frontend.passes

import com.mcbodev.cg.Status
import com.mcbodev.cg.ast.*
import com.mcbodev.cg.exception.error.ControlNotInLoop
import com.mcbodev.cg.exception.error.NotAllPathsReturns
import com.mcbodev.cg.exception.error.ReturnInVoidFunction
import com.mcbodev.cg.lang.PrimitiveType

/**
 *
 */
class Structure(status: Status) : Pass(status) {
    private var insideLoop = false

    /**
     *
     */
    private fun If.structure() {
        var ifReturns = false
        var elseReturns = false
        var everyElifReturns = true

        body.stmt.forEach {
            it.structure()
            ifReturns = ifReturns || it.returns
        }

        elsec?.body?.stmt?.forEach {
            it.structure()
            elseReturns = elseReturns || it.returns
        }

        elifs.forEach {
            var returns = false
            it.body.stmt.forEach { s ->
                s.structure()
                returns = returns || s.returns
            }
            everyElifReturns = everyElifReturns && returns
        }

        returns = ifReturns && elseReturns && everyElifReturns
    }

    /**
     *
     */
    private fun For.structure() {
        insideLoop = true
        body.stmt.forEach {
            it.structure()
        }
        insideLoop = false
    }

    /**
     *
     */
    private fun While.structure() {
        insideLoop = true
        body.stmt.forEach {
            it.structure()
        }
        insideLoop = false
    }

    /**
     *
     */
    private fun Control.structure() {
        if (!insideLoop) {
            throw ControlNotInLoop(this)
        }
    }

    /**
     *
     */
    private fun Statement.structure() {
        when (this) {
            is If -> structure()
            is For -> structure()
            is While -> structure()
            is Control -> structure()
        }
    }

    /**
     *
     */
    private fun FuncDef.structure() {
        var returns = false

        body.stmt.forEach {
            it.structure()
            returns = returns || it.returns
        }

        if (type != PrimitiveType.VOID && !returns) {
            throw NotAllPathsReturns(this)
        } else if (type == PrimitiveType.VOID && returns) {
            throw ReturnInVoidFunction(this)
        }
    }

    /**
     *
     */
    override fun execute(unit: CompilationUnit) {
        unit.funcDefs.forEach {
            guarded { it.structure() }
        }
    }
}
