/*
 * Copyright 2016 Simón Oroño & La Universidad del Zulia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mcbodev.cg.frontend.passes

import com.mcbodev.cg.Status
import com.mcbodev.cg.ast.CompilationUnit
import com.mcbodev.cg.exception.CGCException

/**
 * Pass is the super class for all static check passes
 */
abstract class Pass(val state: Status) {

    /**
     * Executes a given block of code, if some [CGCException] is thrown, it
     * captures and stores it as a compilation error.
     */
    inline fun guarded(f: () -> Unit) {
        try {
            f()
        } catch (e: CGCException) {
            state.errors += e
        }
    }

    /**
     * Executes the static check pass
     */
    abstract fun execute(unit: CompilationUnit)
}
