package com.mcbodev.cg.frontend.passes

import com.mcbodev.cg.Status
import com.mcbodev.cg.ast.*
import com.mcbodev.cg.exception.error.*
import com.mcbodev.cg.lang.ArrayType
import com.mcbodev.cg.lang.Operations
import com.mcbodev.cg.lang.PrimitiveType
import com.mcbodev.cg.symbol.Variable as VariableSym

class Types(status: Status) : Pass(status) {
    private fun ArrayAccess.types() {
        array.types()
        subscript.types()

        if (subscript.type != PrimitiveType.INT) {
            throw ArrayAccessIndexNotInteger(this)
        }

        this.type = array.type
    }

    private fun ArrayLiteral.types() {
        exprs.forEach { it.types() }
        val types = exprs.map { it.type }

        // We can only infer type of non-empty array literals
        if (exprs.isNotEmpty()) {
            val first = types[0]
            val allEqual = types.all { it == first }

            if (!allEqual) {
                throw NonHomogeneousArray(this)
            } else {
                this.type = ArrayType(first)
            }
        } else {
            this.type = PrimitiveType.UNKNOWN
        }
    }

    private fun Assignment.types() {
        lhs.types()
        rhs.types()

        if (!lhs.assignable) {
            throw LHSNotAssignable(this)
        }

        /*
        Odd case: assigning an empty array literal (type UNKNOWN) to a variable
        that may or not have a specified type
         */
        if (rhs is ArrayLiteral && rhs.type == PrimitiveType.UNKNOWN &&
            lhs.type != PrimitiveType.UNKNOWN && lhs.type is ArrayType
        ) {
            rhs.type = lhs.type
        }

        if (lhs.type != rhs.type) {
            throw AssignmentTypeMismatch(this)
        }
    }

    private fun BinaryOp.types() {
        lhs.types()
        rhs.types()

        val res = Operations.getBinaryOpResult(op, lhs.type, rhs.type)
        type =
            res ?: throw BinaryOperationDoesNotExists(this, lhs.type, rhs.type)
    }

    private fun Cast.types() {
        expr.types()

        val res = Operations.getCastResult(target, expr.type)
        type = res ?: throw InvalidCast(this, expr.type)
    }

    private fun Expr.types() {
        when (this) {
            is ArrayAccess -> types()
            is ArrayLiteral -> types()
            is Assignment -> types()
            is BinaryOp -> types()
            is Cast -> types()
            is FuncCall -> types()
            is UnaryOp -> types()
            is Variable -> types()
        }
    }

    private fun FuncCall.types() {
        // TODO
    }

    private fun Statement.types() {
        when (this) {
            is VarDec -> types()
            is Expr -> types()
        }
    }

    private fun UnaryOp.types() {
        expr.types()
        val res = Operations.getUnaryOpResult(op, expr.type)
        type = res ?: throw UnaryOperationDoesNotExists(this, expr.type)
    }

    private fun VarDec.types(global: Boolean = false) {
        if (type == PrimitiveType.UNKNOWN && init == null) {
            throw TypeOfVariableNotInferable(this)
        }

        val qry = state.symTab.findVariable(id, this.parent)

        if (qry == null) {
            val v = VariableSym(id, type, location)
            state.symTab.addVar(v, parent)
        } else {
            throw LocalVariableRedeclaration(qry, this)
        }

        if (!global && init != null) {
            init.types()
        }
    }

    private fun Variable.types() {
        val qry = state.symTab.findVariable(id, parent)

        qry?.let {
            type = qry.type
        } ?: throw VariableNotFound(this)
    }

    private fun FuncDef.types() {
        body.stmt.forEach {
            guarded { it.types() }
        }
    }

    override fun execute(unit: CompilationUnit) {
        unit.varDecs.forEach { it.types(global = true) }

        unit.funcDefs.forEach {
            guarded { it.types() }
        }
    }
}
