/*
 * Copyright 2016 Simón Oroño & La Universidad del Zulia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mcbodev.cg.frontend.passes

import com.mcbodev.cg.Status
import com.mcbodev.cg.ast.CompilationUnit
import com.mcbodev.cg.ast.FuncDef
import com.mcbodev.cg.ast.VarDec
import com.mcbodev.cg.exception.error.FunctionRedefinition
import com.mcbodev.cg.exception.error.GlobalVariableRedeclaration
import com.mcbodev.cg.symbol.Function
import com.mcbodev.cg.symbol.Variable

/**
 * Globals is the static check pass in which all global symbols are registered
 */
class Globals(status: Status) : Pass(status) {

    /**
     * Checks if a variable is already registered; if it's not, registers it.
     */
    private fun VarDec.checkVariable() {
        val qry = state.symTab.findVariable(id, parent)

        if (qry == null) {
            val v = Variable(id, type, location)
            state.symTab.addVar(v, parent)
        } else {
            throw GlobalVariableRedeclaration(qry, this)
        }
    }

    /**
     * Checks if a function is already defined; if it's not, registers it.
     */
    private fun FuncDef.checkFunction() {
        val qry = state.symTab.findFunction(id)

        if (qry == null) {
            val params = params.map {
                Variable(it.id, it.type, it.location)
            }

            val f = Function(id, type, params, location)

            state.symTab.addFn(f)
            params.forEach { state.symTab.addVar(it, this) }
        } else {
            throw FunctionRedefinition(qry, this)
        }
    }

    /**
     * {@inheritDoc}
     */
    override fun execute(unit: CompilationUnit) {
        unit.varDecs.forEach {
            guarded { it.checkVariable() }
        }

        unit.funcDefs.forEach {
            guarded { it.checkFunction() }
        }
    }
}
