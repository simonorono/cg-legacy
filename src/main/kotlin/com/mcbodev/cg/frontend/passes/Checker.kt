/*
 * Copyright 2016 Simón Oroño & La Universidad del Zulia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mcbodev.cg.frontend.passes

import com.mcbodev.cg.Log
import com.mcbodev.cg.Status
import com.mcbodev.cg.ast.CompilationUnit
import java.util.*
import kotlin.system.measureTimeMillis

private typealias PassCnst = (status: Status) -> Pass

class Checker {
    private val passList = LinkedList<PassCnst>()
    private val status = Status()

    private inline fun executePass(cons: PassCnst, ast: CompilationUnit) {
        val pass = cons(status)

        val ms = measureTimeMillis {
            pass.execute(ast)
        }

        Log.println("Pass [${pass.javaClass.simpleName}]: $ms ms")
    }

    operator fun plusAssign(passCnst: PassCnst) {
        passList.add(passCnst)
    }

    fun check(ast: CompilationUnit): Status {
        passList.forEach { executePass(it, ast) }
        return status
    }
}
