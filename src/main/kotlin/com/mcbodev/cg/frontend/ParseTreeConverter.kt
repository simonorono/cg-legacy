/*
 * Copyright 2016 Simón Oroño & La Universidad del Zulia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mcbodev.cg.frontend

import com.mcbodev.cg.ast.*
import com.mcbodev.cg.frontend.internal.CGLexer
import com.mcbodev.cg.frontend.internal.CGParser
import com.mcbodev.cg.lang.ArrayType
import com.mcbodev.cg.lang.Operator
import com.mcbodev.cg.lang.PrimitiveType
import com.mcbodev.cg.lang.Type
import org.antlr.v4.runtime.ParserRuleContext
import org.antlr.v4.runtime.Token

object ParseTreeConverter {
    private val ParserRuleContext.location: Location
        get() {
            val symbol = start
            val p = Point(symbol.line, symbol.charPositionInLine)
            return Location(p)
        }

    private fun CGParser.TypeContext.toCGType(): Type = when {
        this is CGParser.PrimitiveTypeContext -> when (this.start.type) {
            CGLexer.INT -> PrimitiveType.INT
            CGLexer.FLOAT -> PrimitiveType.FLOAT
            CGLexer.BOOL -> PrimitiveType.BOOL
            CGLexer.STRING -> PrimitiveType.STRING
            CGLexer.GRAPH -> PrimitiveType.GRAPH
            CGLexer.DIGRAPH -> PrimitiveType.DIGRAPH
            CGLexer.VOID -> PrimitiveType.VOID

            else -> throw IllegalStateException()
        }
        this is CGParser.ArrayTypeContext -> ArrayType(this.type().toCGType())
        else -> throw IllegalStateException()
    }

    private fun CGParser.LitContext.toAST(): Literal {
        val type = when (this) {
            is CGParser.IntLitContext -> PrimitiveType.INT
            is CGParser.FloatLitContext -> PrimitiveType.FLOAT
            is CGParser.BoolLitContext -> PrimitiveType.BOOL
            is CGParser.StringLitContext -> PrimitiveType.STRING
            else -> throw IllegalStateException()
        }

        val lit = if (this is CGParser.StringLitContext) {
            val t = text.substring(1, text.length - 1)
            Literal(t, location)
        } else {
            Literal(text, location)
        }
        lit.type = type
        return lit
    }

    private fun CGParser.GraphLitContext.toAST(): GraphLit {
        val type = when (gtype.type) {
            CGLexer.GRAPH -> PrimitiveType.GRAPH
            CGLexer.DIGRAPH -> PrimitiveType.DIGRAPH
            else -> throw IllegalStateException()
        }
        val num = num.toAST()
        val edges = edge().map { Edge(it.source.toAST(), it.target.toAST()) }

        val graph = GraphLit(num, edges, location)
        graph.type = type
        return graph
    }

    private fun CGParser.CastExprContext.toAST(): Cast {
        val type = type().toCGType()
        val expr = postfixExpression().toAST()
        return Cast(type, expr, location)
    }

    private fun CGParser.ExprListContext.toAST() = expr().map {
        it.toAST()
    }

    private fun CGParser.ArrayLitContext.toAST(): ArrayLiteral {
        val exprAst = exprList()?.toAST() ?: listOf()
        return ArrayLiteral(exprAst, location)
    }

    private fun CGParser.ReturnContext.toAST(): Return {
        val exprAST = expr().toAST()
        return Return(exprAST, location)
    }

    private fun CGParser.BlockContext.toAST(): Block {
        val stmt = statement().map {
            it.toAST()
        }

        return Block(stmt, location)
    }

    private fun CGParser.ForContext.toAST(): For {
        val loopVar = variable.toAST()
        val collectionVar = collection.toAST()
        val body = block().toAST()

        return For(loopVar, collectionVar, body, location)
    }

    private fun CGParser.WhileContext.toAST(): While {
        val cond = expr().toAST()
        val body = block().toAST()

        return While(cond, body, location)
    }

    private fun CGParser.LoopcContext.toAST(): Statement {
        return when (this) {
            is CGParser.ForContext -> this.toAST()
            is CGParser.WhileContext -> this.toAST()

            else -> throw IllegalStateException()
        }
    }

    private fun CGParser.IfExprContext.toAST(): Statement {
        val ifCond = expr().toAST()
        val ifBody = block().toAST()

        val elifs = elifc().map {
            val elifCond = it.expr().toAST()
            val elifBody = it.block().toAST()
            Elif(elifCond, elifBody)
        }

        val elsec = elsec()?.let {
            val elseBody = it.block().toAST()
            Else(elseBody)
        }

        return If(ifCond, ifBody, elifs, elsec, location)
    }

    private fun CGParser.FunctionCallContext.toAST(): Expr {
        val function = postfixExpression().toAST()
        val arguments = exprList().toAST()
        return FuncCall(function, arguments, location)
    }

    private fun CGParser.PropAccessContext.toAST(): Expr {
        val expr = postfixExpression().toAST()
        val property = IDENTIFIER().text
        return PropAccess(expr, property)
    }

    private fun CGParser.PostfixExpressionContext.toAST(): Expr {
        return when (this) {
            is CGParser.ArrayAccessContext -> toAST()
            is CGParser.FunctionCallContext -> toAST()
            is CGParser.CastExprContext -> toAST()
            is CGParser.PropAccessContext -> toAST()
            is CGParser.AtomicContext -> atom().toAST()

            else -> throw IllegalStateException()
        }
    }

    private fun CGParser.AtomContext.toAST(): Expr {
        return when (this) {
            is CGParser.LiteralExprContext -> lit().toAST()
            is CGParser.VarExprContext -> Variable(IDENTIFIER().text, location)
            is CGParser.GraphExprContext -> graphLit().toAST()
            is CGParser.ArrayExprContext -> arrayLit().toAST()

            else -> throw IllegalStateException()
        }
    }

    private fun CGParser.ArrayAccessContext.toAST(): Expr {
        val array = postfixExpression().toAST()
        val subscript = expr().toAST()
        return ArrayAccess(array, subscript, location)
    }

    private fun CGParser.UnaryContext.toAST(): Expr {
        val operator = Operator.fromToken(op.type)
        val operand = expr().toAST()
        return UnaryOp(operator, operand, location)
    }

    private fun ParserRuleContext.binary(
        op: Token,
        ex1: CGParser.ExprContext,
        ex2: CGParser.ExprContext
    ): BinaryOp {
        val lhs = ex1.toAST()
        val rhs = ex2.toAST()
        val o = Operator.fromToken(op.type)
        return BinaryOp(o, lhs, rhs, location)
    }

    private fun CGParser.MulDivModContext.toAST(): Expr =
        binary(op, expr(0), expr(1))

    private fun CGParser.AddSubContext.toAST(): Expr =
        binary(op, expr(0), expr(1))

    private fun CGParser.ComparisonContext.toAST(): Expr =
        binary(op, expr(0), expr(1))

    private fun CGParser.EqualityContext.toAST(): Expr =
        binary(op, expr(0), expr(1))

    private fun CGParser.LogicAndContext.toAST(): Expr =
        binary(op, expr(0), expr(1))

    private fun CGParser.LogicOrContext.toAST(): Expr =
        binary(op, expr(0), expr(1))

    private fun CGParser.AssignmentContext.toAST(): Expr {
        val lhs = expr(0).toAST()
        val rhs = expr(1).toAST()
        return Assignment(lhs, rhs, location)
    }

    private fun CGParser.AssignmentAccContext.toAST(): Expr {
        val lhs = expr(0).toAST()
        val rhs = expr(1).toAST()
        val op = Operator.fromToken(op.type)

        val realOP = when (op) {
            Operator.ADD_EQUAL -> Operator.ADD
            Operator.SUB_EQUAL -> Operator.SUB
            Operator.MUL_EQUAL -> Operator.MUL
            Operator.DIV_EQUAL -> Operator.DIV

            else -> throw IllegalStateException()
        }

        // TODO: fix location
        val v = BinaryOp(realOP, lhs, rhs, location)
        return Assignment(lhs, v, location)
    }

    private fun CGParser.RangeContext.toAST(): Expr {
        val from = from.toAST()
        val to = to.toAST()
        return Range(from, to, location)
    }

    private fun CGParser.ExprContext.toAST(): Expr {
        return when (this) {
            is CGParser.PostfixContext -> postfixExpression().toAST()
            is CGParser.UnaryContext -> toAST()
            is CGParser.AssocContext -> expr().toAST()
            is CGParser.MulDivModContext -> toAST()
            is CGParser.AddSubContext -> toAST()
            is CGParser.ComparisonContext -> toAST()
            is CGParser.EqualityContext -> toAST()
            is CGParser.LogicAndContext -> toAST()
            is CGParser.LogicOrContext -> toAST()
            is CGParser.AssignmentContext -> toAST()
            is CGParser.AssignmentAccContext -> toAST()
            is CGParser.RangeContext -> toAST()

            else -> throw IllegalStateException()
        }
    }

    private fun CGParser.VarDecContext.toAST(): VarDec {
        val id = IDENTIFIER().text
        val type = type()?.toCGType() ?: PrimitiveType.UNKNOWN
        val init = expr()?.toAST()
        return VarDec(id, type, init, location)
    }

    private fun CGParser.ControlContext.toAST(): Control {
        val type = when (start.type) {
            CGLexer.CONTINUE -> ControlType.CONTINUE
            CGLexer.BREAK -> ControlType.BREAK
            else -> throw IllegalStateException()
        }

        return Control(type, location)
    }

    private fun CGParser.CompoundStmtContext.toAST(): Statement {
        val cmp = compound()
        return when (cmp) {
            is CGParser.IfExprContext -> cmp.toAST()
            is CGParser.LoopExprContext -> cmp.loopc().toAST()

            else -> throw IllegalStateException()
        }
    }

    private fun CGParser.StatementContext.toAST(): Statement {
        return when (this) {
            is CGParser.LocalVarDecContext -> varDec().toAST()
            is CGParser.StmtContext -> expr().toAST()
            is CGParser.ReturnContext -> toAST()
            is CGParser.ControlContext -> toAST()
            is CGParser.CompoundStmtContext -> toAST()

            else -> throw IllegalStateException()
        }
    }

    private fun CGParser.ParamContext.toAST(): Param {
        val id = IDENTIFIER().text
        val type = type().toCGType()
        return Param(id, type, location)
    }

    private fun CGParser.FuncDefContext.toAST(): FuncDef {
        val id = IDENTIFIER().text
        val params = paramList().param().map { it.toAST() }
        var type = type()?.toCGType() ?: PrimitiveType.VOID

        val block = block()
        val expr = expr()

        val body = when {
            block != null -> block.toAST()
            expr != null -> {
                type = PrimitiveType.UNKNOWN
                val ret = Return(expr.toAST())
                Block(listOf(ret))
            }
            else -> throw IllegalStateException()
        }

        return FuncDef(id, params, type, body, location)
    }

    fun toAST(init: CGParser.UnitContext): CompilationUnit {
        val funcDefs = init.funcDef().map { it.toAST() }
        val varDecs = init.varDec().map { it.toAST() }

        return CompilationUnit(funcDefs, varDecs, init.location)
    }
}
