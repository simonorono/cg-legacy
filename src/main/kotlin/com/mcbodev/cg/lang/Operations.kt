package com.mcbodev.cg.lang

private data class BinaryOp(
    val op: Operator,
    val lhs: Type,
    val rhs: Type,
    val result: Type
) {
    constructor(op: Operator, type: Type) : this(op, type, type, type)
    constructor(op: Operator, args: Type, result: Type) : this(
        op,
        args,
        args,
        result
    )

    companion object {
        fun fromOpList(op: List<Operator>, type: Type) = op.map {
            BinaryOp(it, type)
        }

        fun fromOpList(op: List<Operator>, args: Type, result: Type) = op.map {
            BinaryOp(it, args, result)
        }
    }
}

private data class Cast(val target: Type, val source: Type) {
    constructor(type: Type) : this(type, type)

    companion object {
        fun fromTypeList(sources: List<Type>, target: Type) = sources.map {
            Cast(target, it)
        }
    }
}

private data class UnaryOp(
    val op: Operator,
    val type: Type,
    val result: Type
) {
    constructor(op: Operator, type: Type) : this(op, type, type)
}

object Operations {
    private val binaryOperations = HashSet<BinaryOp>()
    private val casts = HashSet<Cast>()
    private val unaryOperations = HashSet<UnaryOp>()

    init {
        val intMath =
            BinaryOp.fromOpList(Operator.mathOperators, PrimitiveType.INT)

        val floatMath =
            BinaryOp.fromOpList(Operator.mathOperators, PrimitiveType.FLOAT)

        val intComp = BinaryOp.fromOpList(
            Operator.comparisonOperators,
            PrimitiveType.INT,
            PrimitiveType.BOOL
        )

        val floatComp = BinaryOp.fromOpList(
            Operator.comparisonOperators,
            PrimitiveType.FLOAT,
            PrimitiveType.BOOL
        )

        val stringComp = BinaryOp.fromOpList(
            Operator.comparisonOperators,
            PrimitiveType.STRING,
            PrimitiveType.BOOL
        )

        val boolComp = BinaryOp.fromOpList(
            Operator.comparisonOperators,
            PrimitiveType.BOOL
        )

        binaryOperations += intMath
        binaryOperations += floatMath
        binaryOperations += intComp
        binaryOperations += floatComp
        binaryOperations += stringComp
        binaryOperations += boolComp

        unaryOperations += UnaryOp(Operator.NOT, PrimitiveType.BOOL)
        unaryOperations += UnaryOp(Operator.SUB, PrimitiveType.INT)
        unaryOperations += UnaryOp(Operator.SUB, PrimitiveType.FLOAT)

        casts += PrimitiveType.values().map { Cast(it) }

        val types =
            listOf(PrimitiveType.INT, PrimitiveType.FLOAT, PrimitiveType.BOOL)
        casts + Cast.fromTypeList(types, PrimitiveType.STRING)
    }

    fun getBinaryOpResult(op: Operator, lhs: Type, rhs: Type): Type? =
        binaryOperations.firstOrNull { it.op == op && it.lhs == lhs && it.rhs == rhs }?.result

    fun getUnaryOpResult(op: Operator, type: Type): Type? =
        unaryOperations.firstOrNull { it.op == op && it.type == type }?.result

    fun getCastResult(target: Type, source: Type) =
        casts.firstOrNull { it.target == target && it.source == source }?.target
}
