/*
 * Copyright 2016 Simón Oroño & La Universidad del Zulia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mcbodev.cg.lang

interface Type

enum class PrimitiveType : Type {
    INT,
    FLOAT,
    BOOL,
    STRING,
    GRAPH,
    DIGRAPH,
    VOID,

    ERROR,
    UNKNOWN;

    override fun toString() = when (this) {
        INT -> "int"
        FLOAT -> "float"
        BOOL -> "bool"
        STRING -> "string"
        GRAPH -> "graph"
        DIGRAPH -> "digraph"
        VOID -> "void"
        ERROR -> "ERROR"
        UNKNOWN -> "UNKNOWN"
    }
}

data class ArrayType(val inner: Type) : Type {
    override fun toString() = "[$inner]"

    override fun equals(other: Any?) = when (other) {
        is ArrayType -> inner == other.inner
        else -> false
    }

    override fun hashCode(): Int {
        return inner.hashCode() * 37
    }
}

// Not used by the moment
//data class UserType(val id: String) : Type
