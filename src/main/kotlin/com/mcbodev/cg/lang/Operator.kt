/*
 * Copyright 2016 Simón Oroño & La Universidad del Zulia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mcbodev.cg.lang

import com.mcbodev.cg.frontend.internal.CGLexer

enum class Operator {
    ADD,
    SUB,
    MUL,
    DIV,
    MOD,

    NOT,

    EQUAL,
    EQUAL_EQUAL,
    NOT_EQUAL,

    GREATER,
    LESS,
    GREATER_EQUAL,
    LESS_EQUAL,

    AND,
    OR,

    ADD_EQUAL,
    SUB_EQUAL,
    MUL_EQUAL,
    DIV_EQUAL;

    override fun toString() = when (this) {
        ADD -> "+"
        SUB -> "-"
        MUL -> "*"
        DIV -> "/"
        MOD -> "%"
        NOT -> "!"
        EQUAL -> "="
        EQUAL_EQUAL -> "=="
        NOT_EQUAL -> "!="
        GREATER -> ">"
        LESS -> "<"
        GREATER_EQUAL -> ">="
        LESS_EQUAL -> "<="
        AND -> "&&"
        OR -> "||"
        ADD_EQUAL -> "+="
        SUB_EQUAL -> "-="
        MUL_EQUAL -> "*="
        DIV_EQUAL -> "/="
    }

    companion object {
        fun fromToken(token: Int) = when (token) {
            CGLexer.ADD -> ADD
            CGLexer.SUB -> SUB
            CGLexer.MUL -> MUL
            CGLexer.DIV -> DIV
            CGLexer.MOD -> MOD
            CGLexer.BANG -> NOT
            CGLexer.EQUAL -> EQUAL
            CGLexer.EQUAL_EQUAL -> EQUAL_EQUAL
            CGLexer.NOT_EQUAL -> NOT_EQUAL
            CGLexer.GT -> GREATER
            CGLexer.LT -> LESS
            CGLexer.GE -> GREATER_EQUAL
            CGLexer.LE -> LESS_EQUAL
            CGLexer.AND -> AND
            CGLexer.OR -> OR

            else -> throw IllegalStateException()
        }

        val mathOperators = listOf(
            Operator.ADD,
            Operator.SUB,
            Operator.MUL,
            Operator.DIV,
            Operator.MOD
        )

        val comparisonOperators = listOf(
            EQUAL_EQUAL,
            NOT_EQUAL,
            GREATER,
            LESS,
            GREATER_EQUAL,
            LESS_EQUAL
        )
    }
}
