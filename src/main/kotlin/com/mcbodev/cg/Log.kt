/**
 *
 */

package com.mcbodev.cg

import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

object Log {
    private val FMT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
    var debug = false

    fun println(msg: String) {
        if (debug) {
            val date = FMT.format(ZonedDateTime.now())
            kotlin.io.println("[$date] $msg")
        }
    }
}
