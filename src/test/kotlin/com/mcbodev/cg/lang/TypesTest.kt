package com.mcbodev.cg.lang

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class TypesTest {
    @Test
    fun `primitive types`() {
        PrimitiveType.values().forEach {
            assertThat(it.toString())
                .isNotNull()
                .isNotBlank()
                .isNotEmpty()
        }
    }

    @Test
    fun `array types`() {
        val intArray = ArrayType(PrimitiveType.INT)
        val floatArray = ArrayType(PrimitiveType.FLOAT)
        val anotherIntArray = ArrayType(PrimitiveType.INT)

        assertThat(intArray)
            .isEqualTo(anotherIntArray)
            .isNotEqualTo(floatArray)
            .isNotEqualTo(PrimitiveType.INT)

        assertThat(intArray.hashCode())
            .isEqualTo(anotherIntArray.hashCode())
            .isNotEqualTo(floatArray.hashCode())
            .isNotEqualTo(PrimitiveType.INT.hashCode())

        assertThat(intArray.toString())
            .isEqualTo("[${PrimitiveType.INT}]")

        assertThat(floatArray.toString())
            .isEqualTo("[${PrimitiveType.FLOAT}]")
    }
}
