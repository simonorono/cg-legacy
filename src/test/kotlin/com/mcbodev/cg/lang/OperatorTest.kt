package com.mcbodev.cg.lang

import com.mcbodev.cg.frontend.internal.CGLexer
import org.assertj.core.api.Assertions.*
import org.junit.jupiter.api.Test

class OperatorTest {
    @Test
    fun `operator test`() {
        Operator.values().forEach {
            assertThat(it.toString())
                .isNotNull()
                .isNotBlank()
                .isNotEmpty()
        }
    }

    @Test
    fun `conversion from lexemes to operators`() {
        val tokens = arrayOf(
            CGLexer.ADD, CGLexer.SUB, CGLexer.MUL, CGLexer.DIV, CGLexer.MOD,
            CGLexer.BANG, CGLexer.EQUAL, CGLexer.EQUAL_EQUAL, CGLexer.NOT_EQUAL,
            CGLexer.GT, CGLexer.LT, CGLexer.GE, CGLexer.LE, CGLexer.AND,
            CGLexer.OR
        )

        assertThatCode {
            tokens.map { Operator.fromToken(it) }
        }.doesNotThrowAnyException()

        assertThatExceptionOfType(IllegalStateException::class.java).isThrownBy {
            Operator.fromToken(CGLexer.FLOAT)
        }
    }
}
