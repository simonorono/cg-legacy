package com.mcbodev.cg.lang

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class OperationsTest {
    @Test
    fun `unary operations`() {
        val boolNegRes =
            Operations.getUnaryOpResult(Operator.NOT, PrimitiveType.BOOL)

        assertThat(boolNegRes)
            .isNotNull
            .isEqualTo(PrimitiveType.BOOL)
    }

    @Test
    fun `binary operations`() {
        val intAdditionRes = Operations.getBinaryOpResult(
            Operator.ADD,
            PrimitiveType.INT,
            PrimitiveType.INT
        )

        val intFloatAddition = Operations.getBinaryOpResult(
            Operator.ADD,
            PrimitiveType.INT,
            PrimitiveType.FLOAT
        )

        assertThat(intAdditionRes)
            .isNotNull
            .isEqualTo(PrimitiveType.INT)

        assertThat(intFloatAddition)
            .isNull()
    }
}
