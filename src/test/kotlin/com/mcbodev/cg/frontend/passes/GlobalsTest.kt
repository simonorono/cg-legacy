package com.mcbodev.cg.frontend.passes

import com.mcbodev.cg.exception.error.FunctionRedefinition
import com.mcbodev.cg.exception.error.GlobalVariableRedeclaration
import com.mcbodev.cg.helper.toAST
import com.mcbodev.cg.lang.PrimitiveType
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class GlobalsTest {
    private val checker = Checker()

    init {
        checker += ::Globals
    }

    @Test
    fun `basic test`() {
        val ast = """
            func add(a int, b int) = a + b;
            var c = 1;
        """.trimIndent().toAST()
        val status = checker.check(ast)

        val function = status.symTab.findFunction("add")

        assertThat(function).isNotNull
        assertThat(status.symTab.findVariable("c", "main")).isNotNull

        assertThat(function?.let { it.params.map { p -> p.id } })
            .isNotNull
            .contains("a", "b")

        assertThat(function?.let { it.params.map { p -> p.type } })
            .isNotNull
            .containsOnly(PrimitiveType.INT)
    }

    @Test
    fun `redeclaration errors`() {
        val ast = """
            func add(a int, b int) = a + b;
            func add(a int, b int) = a + b;
            var c = 1;
            var c = 1;
        """.trimIndent().toAST()
        val status = checker.check(ast)

        val errorsType = status.errors.map { it::class }
        assertThat(errorsType)
            .contains(GlobalVariableRedeclaration::class)
            .contains(FunctionRedefinition::class)
    }


}
