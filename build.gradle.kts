/*
 * Copyright 2016 Simón Oroño & La Universidad del Zulia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

plugins {
    antlr
    application
    java
    kotlin("jvm") version "1.3.20"

    id("org.jetbrains.dokka") version "0.9.17"
}

val mainClass = "com.mcbodev.cg.MainKt"

application {
    mainClassName = mainClass
}

repositories {
    jcenter()
    mavenCentral()
}

val junitVersion = "5.3.2"

dependencies {
    antlr("org.antlr:antlr4:4.7.2")
    implementation("commons-cli:commons-cli:1.4")
    implementation(kotlin("stdlib-jdk8"))

    testImplementation("org.assertj:assertj-core:3.11.1")
    testImplementation("org.junit.jupiter:junit-jupiter-api:$junitVersion")
    testRuntime("org.junit.jupiter:junit-jupiter-engine:$junitVersion")
}

tasks.compileKotlin {
    sourceCompatibility = "1.8"
    targetCompatibility = "1.8"
    dependsOn.add(tasks.generateGrammarSource)
}

tasks.compileTestKotlin {
    sourceCompatibility = "1.8"
    targetCompatibility = "1.8"
    dependsOn.add(tasks.generateGrammarSource)
}

tasks.generateGrammarSource {
    val args = listOf("-package", "com.mcbodev.cg.frontend.internal")
    arguments.addAll(args)
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<Jar> {
    manifest {
        attributes["Main-Class"] = mainClass
    }

    configurations["compileClasspath"].forEach {
        from(zipTree(it.absoluteFile))
    }
}
